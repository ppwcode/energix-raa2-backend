using System.Net;

using Energix.Common.Tests.Azure.Functions;
using Energix.RAA2.Service.Functions.I;

using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging.Abstractions;

using Xunit;

namespace Energix.RAA2.Service.Tests.I
{
    public class HealthUnitTest : BaseFunctionTest
    {
        private readonly Health _function;

        public HealthUnitTest()
        {
            _function = new Health(NullLoggerFactory.Instance);
        }

        [Fact]
        public void HealthGetShouldReturnOkTest()
        {
            // Arrange
            HttpRequestData request =
                new FakeHttpRequestData(
                    FunctionContext,
                    "does_not_care",
                    "does_not_care");

            // Act
            HttpResponseData response = _function.Get(request);

            // Assert
            Assert.NotNull(response);
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);
        }
    }
}
