using Microsoft.Extensions.Hosting;

namespace Energix.RAA2.Service
{
    public static class Program
    {
        public static void Main()
        {
            IHost host = new HostBuilder()
                .ConfigureFunctionsWorkerDefaults()
                .Build();

            host.Run();
        }
    }
}
