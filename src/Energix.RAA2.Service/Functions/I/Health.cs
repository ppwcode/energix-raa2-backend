using System.Net;

using Microsoft.Azure.Functions.Worker;
using Microsoft.Azure.Functions.Worker.Http;
using Microsoft.Extensions.Logging;

namespace Energix.RAA2.Service.Functions.I
{
    public class Health
    {
        private readonly ILogger<Health> _logger;

        public Health(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.CreateLogger<Health>();
        }

        [Function(nameof(Health) + nameof(Get))]
        public HttpResponseData Get([HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = "I/health")] HttpRequestData req)
        {
            _logger.LogInformation("C# HTTP trigger function processed a request.");

            HttpResponseData response = req.CreateResponse(HttpStatusCode.OK);

            return response;
        }
    }
}
