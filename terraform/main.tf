# Configure Terraform
terraform {
  required_providers {
    azuread = {
      source  = "hashicorp/azuread"
      version = "~> 2.15.0"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=3.0.2"
    }
  }
}

provider "azurerm" {
  features {}

  storage_use_azuread = true
  subscription_id     = local.subscription_id
}

data "azurerm_resource_group" "energix" {
  name                = local.resource_group_name
}

resource "azurerm_application_insights" "energix-raa2-application-insights" {
  name                = "energix-raa2-application-insights"
  location            = local.location
  resource_group_name = local.resource_group_name
  workspace_id        = data.terraform_remote_state.aad.outputs.I-log-analytics.id
  application_type    = "web"
}


resource "azurerm_service_plan" "energix-raa2-service-plan" {
  name                = "energix-raa2-service-plan"
  location            = local.location
  resource_group_name = local.resource_group_name
  os_type             = "Windows"
  sku_name            = "Y1"
}

resource "azurerm_storage_account" "energix-raa2-storage-account" {
  name                     = "energixraa2storage"
  location                 = local.location
  resource_group_name      = local.resource_group_name
  account_tier             = "Standard"
  account_replication_type = "LRS"
  account_kind             = "StorageV2"
  queue_properties {

    logging {
      delete                = true
      read                  = true
      write                 = true
      version               = "1.0"
      retention_policy_days = 10
    }

    hour_metrics {
      enabled               = true
      include_apis          = true
      version               = "1.0"
      retention_policy_days = 10
    }

    minute_metrics {
      enabled               = true
      include_apis          = true
      version               = "1.0"
      retention_policy_days = 10
    }
  }
}

# Create blob container for Raa2 service versions (in .zip format)
resource "azurerm_storage_container" "energix-raa2-container" {
  name                  = "azure-functions-versions"
  storage_account_name  = azurerm_storage_account.energix-raa2-storage-account.name
  container_access_type = "private"
}

###################################################################################################################
# The user needs to have "Application Administrator" role to be allowed to execute the rest of the configuration! #
###################################################################################################################

# Get current client configuration
data "azuread_client_config" "current" {}

# Create an application
resource "azuread_application" "application" {
  display_name = "raa2_service_principal"
  owners       = [data.azuread_client_config.current.object_id]
}

# Create a service principal
resource "azuread_service_principal" "service-principal" {
  application_id = azuread_application.application.application_id
  owners         = [data.azuread_client_config.current.object_id]
}

# Create a password for the service principal
resource "azuread_service_principal_password" "password" {
  service_principal_id = azuread_service_principal.service-principal.object_id
}

# Create role "Contributor" on the Resource Group (to create and update function app's)
resource "azurerm_role_assignment" "add-role-contributor-on-rg" {
  scope                = data.azurerm_resource_group.energix.id
  role_definition_name = "Contributor"
  principal_id         = azuread_service_principal.service-principal.id
}

# Create role "Storage Blob Data Contributor" on the Storage Account (to upload zip's)
resource "azurerm_role_assignment" "add-role-storage_blob_data_contributor-on-sa" {
  scope                = azurerm_storage_account.energix-raa2-storage-account.id
  role_definition_name = "Storage Blob Data Contributor"
  principal_id         = azuread_service_principal.service-principal.id
}

# tenant_id
output "service_principal_tenant_id" {
  value = "${data.azuread_client_config.current.tenant_id}"
  sensitive = false
}

# app_id
output "service_principal_app_id" {
  value = "${azuread_service_principal.service-principal.application_id}"
  sensitive = false
}

# app_secret
output "service_principal_password" {
  value = "${azuread_service_principal_password.password.value}"
  sensitive = true
}






