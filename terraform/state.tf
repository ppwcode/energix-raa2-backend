terraform {
  backend "azurerm" {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstateenergix"
    container_name       = "tfstate-energix"
    key                  = "energix-raa2-backend-terraform.tfstate"
    subscription_id      = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
    use_azuread_auth     = true
  }
}

data "terraform_remote_state" "aad" {
  backend = "azurerm"
  config = {
    resource_group_name  = "tfstate"
    storage_account_name = "tfstateenergix"
    container_name       = "tfstate-energix"
    key                  = "energix-aad-terraform.tfstate"
    subscription_id      = "098ad191-f2fd-4240-9a7c-e269ab8394c0"
    use_azuread_auth     = true
  }
}
